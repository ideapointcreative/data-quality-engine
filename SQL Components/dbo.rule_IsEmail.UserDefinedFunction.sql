USE [DQ]
GO
/****** Object:  UserDefinedFunction [dbo].[rule_IsEmail]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rule_IsEmail](
	@TestValue	varchar(1000)
)
RETURNS INT
AS
BEGIN
	-- Clean the value up
	SET @TestValue = TRIM(@TestValue)
	
	-- Verify No Spaces exist
	IF (CHARINDEX(' ',@TestValue,0) > 0)
		RETURN 0	
	
	-- Test Email Length
	IF (LEN(@TestValue) > 255)
		RETURN 0

	-- Test Email format
	IF (@TestValue LIKE '%_@_%.__%')
		RETURN 1

	RETURN 0
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Function will test the formatting of an email address to ensure it complies with standard lengths and formatting.  This test does not ensure the validity or existence of the email address, only that it appears to be valid.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'rule_IsEmail'
GO
