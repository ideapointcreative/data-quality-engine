USE [DQ]
GO
/****** Object:  Table [dbo].[Execution]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Execution](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Measure_ID] [bigint] NULL,
	[TimeStamp] [datetime] NULL,
	[Assertion] [varchar](max) NULL,
	[ViolationCount] [int] NOT NULL,
	[ViolationData] [xml] NULL,
	[NotificationSent] [bit] NOT NULL,
 CONSTRAINT [PK_Measure] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Execution] ADD  DEFAULT (getdate()) FOR [TimeStamp]
GO
ALTER TABLE [dbo].[Execution] ADD  DEFAULT ((0)) FOR [ViolationCount]
GO
ALTER TABLE [dbo].[Execution] ADD  CONSTRAINT [DF_Execution_Notified]  DEFAULT ((0)) FOR [NotificationSent]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity of the Execution' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Foreign key to the rule which was executed.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution', @level2type=N'COLUMN',@level2name=N'Measure_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Time and date in which the rule was run.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution', @level2type=N'COLUMN',@level2name=N'TimeStamp'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The Test SQL which was run during the test.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution', @level2type=N'COLUMN',@level2name=N'Assertion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The total number of rows which were returned by the test, these are the rule "violators".' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution', @level2type=N'COLUMN',@level2name=N'ViolationCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'XML representation of the violating rules.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution', @level2type=N'COLUMN',@level2name=N'ViolationData'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Contains all of the results of rule executions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Execution'
GO
