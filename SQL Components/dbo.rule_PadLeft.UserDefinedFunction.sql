USE [DQ]
GO
/****** Object:  UserDefinedFunction [dbo].[rule_PadLeft]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rule_PadLeft](
	@TestValue	varchar(255),
	@PadChar	char(1) = 0,
	@Length		int
)
RETURNS varchar(100)
AS
BEGIN
	DECLARE @ReturnValue AS VARCHAR(255) 
	SET @ReturnValue = REPLICATE(@PadChar,255)	
	SET @ReturnValue = RIGHT(@ReturnValue+ISNULL(@TestValue,''),@Length)
	RETURN @ReturnValue
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This rule will pad the left side of a value with the requested character to fill a defined length.

SELECT DQ.rule_PadLeft(''1234'',''0'',10)
0000001234' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'rule_PadLeft'
GO
