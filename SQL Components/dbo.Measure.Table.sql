USE [DQ]
GO
/****** Object:  Table [dbo].[Measure]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Measure](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Description] [varchar](250) NULL,
	[Threshold] [numeric](5, 3) NULL,
	[Category_ID] [bigint] NULL,
	[NotificationSMTP] [varchar](1000) NULL,
	[Assertion] [varchar](max) NULL,
	[Disable] [bit] NOT NULL,
	[FailureCount] [int] NOT NULL,
	[DatasetCount] [varchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Measure] ADD  DEFAULT ((0.000)) FOR [Threshold]
GO
ALTER TABLE [dbo].[Measure] ADD  DEFAULT ((0)) FOR [Disable]
GO
ALTER TABLE [dbo].[Measure] ADD  DEFAULT ((0)) FOR [FailureCount]
GO
ALTER TABLE [dbo].[Measure]  WITH CHECK ADD  CONSTRAINT [FK_Measure_Category] FOREIGN KEY([Category_ID])
REFERENCES [dbo].[Category] ([ID])
GO
ALTER TABLE [dbo].[Measure] CHECK CONSTRAINT [FK_Measure_Category]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Identity of the rule.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Friendly name for the Measure' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'Name'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Detailed description of the Measure.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'Description'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The threshold that should not be exceeded for the Measure. 

If the THRESHOLD is to be a set number of violations, then just enter a whole number (e.g. 10, 100, 150)

If the THRESHOLD is to be a percentage of the total records, then enter that percentage (e.g. .10. .001, .005).  If a percentage is used, a SQL statement is required by be in the DATASETCOUNT field, so that the engine can determine the total number of records.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'Threshold'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Category of the Measure.  This is meant to allow grouping of the Measures as required for the business needs.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'Category_ID'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The SMTP address(es) to notify when the measure''s threshold has been exceeded.  If entering multiple addresses, separate each with a semicolon.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'NotificationSMTP'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'The TSQL of the Data Quality Test.  The SQL should execute successfully from the context of the DQ database.  This MUST be a SELECT statement, not a call to a stored procedure.

If the database being checked is on the same server, utilize three part naming (database.schema.object)

If the database is on a linked server use the four-part naming convention (server.database.schema.object)' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'Assertion'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Simple flag that allows a Measure to be enabled or disabled.  Default is enabled (0).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'Disable'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'If a Measure fails during its execution, this number is incremented.  The engine (usp_ExecuteMeasure) will automatically disable a rule after 5 consecutive failures. When this happens, a reason will be noted in [DisableReason].
When a Measure runs successfully, this will be reset to zero (0).' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'FailureCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This is an optional field, which is used when you want the THRESHOLD to function as a percentage of the violations to the total available dataset.

The SQL here, should return a single numeric value, which represents the total of which the Assertion was run against.

If there is no DATASET count logic, then the THRESHOLD is considered a count of violations, and not a percent of violations.

For Instance if your ASSERTION is
  SELECT userid FROM t_users WHERE dbo.rule_IsEmpty(fname) = 1
Your DATASETCOUNT would be
  SELECT COUNT(ID) AS exp1 FROM t_users' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure', @level2type=N'COLUMN',@level2name=N'DatasetCount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Table contains the rule definitions.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Measure'
GO
