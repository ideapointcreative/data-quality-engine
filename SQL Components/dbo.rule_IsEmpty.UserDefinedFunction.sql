USE [DQ]
GO
/****** Object:  UserDefinedFunction [dbo].[rule_IsEmpty]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rule_IsEmpty](
	@TestValue	varchar(100)
)
RETURNS INT
AS
BEGIN
	-- Clean the value up
	SET @TestValue = TRIM(@TestValue)

	IF (LEN(@TestValue) > 0)
		RETURN 0

	RETURN 1
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Tests a field to see if it is empty.  If the field is empty, null, or filled with spaces a 1 will be returned.  If there is data in the field, a 0 will be returned.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'rule_IsEmpty'
GO
