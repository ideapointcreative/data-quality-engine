USE [DQ]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExtractViolations]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		JK, DF
-- Create date: 2020-07-13
-- Description:	Extracts the violation data for a particular execution
--				in tabular format.

-- BUG: Not correctly handling fields when null.
-- =============================================
CREATE PROCEDURE [dbo].[usp_ExtractViolations] 
	@Execution_ID	BIGINT = 0
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE		@ViolationData		AS XML,
				@ColumnCount		AS INT
	SELECT		@ViolationData		= ViolationData FROM Execution WHERE ID = @Execution_ID

	IF OBJECT_ID('tempdb..#XML') IS NOT NULL DROP TABLE #XML
	SELECT		@Execution_ID AS Execution_ID, 
				[NodeName], 
				[Value], 
				ROW_NUMBER() OVER (PARTITION BY NodeName ORDER BY NodeName) AS ID
	INTO		#XML
	FROM		[dbo].[XMLTable](@ViolationData)
	WHERE		ParentName = 'row'

	--Dynamic Pivot table
	DECLARE		@cols				AS NVARCHAR(MAX),
				@query				AS NVARCHAR(MAX);

	--SQL that writes to SQL in order to create a dynamic range of columns
	SET			@cols				= STUFF(
										(
											SELECT		',' + QUOTENAME(c.NodeName)
											FROM		#XML AS c
											GROUP BY c.NodeName
											--ORDER BY c.NodeName
										FOR XML PATH(''), 
											TYPE).value('.', 'NVARCHAR(MAX)'), 1, 1, ''
									  )
	--PRINT @cols

	--This is the actual pivot with our dynamic columns
	SET			@query				= 'SELECT  ' + @cols + '
													FROM (
											SELECT Execution_ID, NodeName, [Value], ID
										FROM #XML
										) src
										PIVOT (
											MAX([Value])
										FOR NodeName IN (' + @cols + ')
										) piv;'

	EXECUTE sp_executesql @query, N'@cols NVARCHAR(MAX) OUTPUT', @cols = @cols OUTPUT

END
GO
