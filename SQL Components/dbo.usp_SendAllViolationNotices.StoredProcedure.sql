USE [DQ]
GO
/****** Object:  StoredProcedure [dbo].[usp_SendAllViolationNotices]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SendAllViolationNotices]
AS
BEGIN
	SET NOCOUNT ON;

	-- Load Rules into Cursor
	-- =============================================================
	DECLARE		@Execution_ID BIGINT
	DECLARE c1 CURSOR FOR
		SELECT	ID FROM	dbo.[Execution] WHERE ViolationCount > 0 and NotificationSent = 0

	-- Loop Through Cursor
	-- =============================================================
	OPEN c1
	FETCH NEXT FROM c1 INTO @Execution_ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		Print 'Processing Execution_ID: ' + CAST(@Execution_ID AS VARCHAR(25))
		EXEC [usp_SendViolationNotice] @Execution_ID
		
		UPDATE		dbo.[Execution] 
		SET			NotificationSent = 1
		WHERE		ID = @Execution_ID

		FETCH NEXT FROM c1 INTO @Execution_ID
	END
	CLOSE c1
	DEALLOCATE c1

END

GO
