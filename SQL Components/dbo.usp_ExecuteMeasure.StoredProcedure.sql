USE [DQ]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExecuteMeasure]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE	Procedure [dbo].[usp_ExecuteMeasure] (
	@Measure_ID		bigint
	)
AS
BEGIN
	-- 2020-07-13 JK: Added "ELEMENTS XSINIL" to @SQLstatement to ensure NULL value fields get generated in the XML
	--			Reference: https://docs.microsoft.com/en-us/sql/relational-databases/xml/columns-that-contain-a-null-value-by-default?view=sql-server-ver15
	
	DECLARE		@temp_XML		XML,
				@dynamic_SQL	nvarchar(4000),
				@Assertion		nvarchar(4000),
				@FailureCount	INT,
				@Disable		BIT = 0

	-- Retrieve Measure Details
	-- ========================================================
	SELECT	@Assertion = Assertion, @FailureCount = FailureCount
	FROM	dbo.[Measure]
	WHERE	ID = @Measure_ID AND Disable = 0

	-- Run the test dump results to XML variable(@temp_XML)
	-- ========================================================
	DECLARE @SQLstatement			NVARCHAR(max)	= N'SELECT @result = ('+ @Assertion +' FOR XML RAW, ELEMENTS XSINIL)',
			@ParameterDefinition	NVARCHAR(1000)	= N'@result XML OUTPUT',
			@TestResultCount		INT				= 0,
			@Execution_ID			BIGINT			= 0
	PRINT @SQLstatement
	BEGIN TRY
		EXECUTE sp_executesql		@SQLstatement, @ParameterDefinition, @result=@temp_XML OUTPUT
		
		-- Determine Row Count
		SELECT @TestResultCount = ISNULL(@temp_XML.value('count(/row)', 'int'),0)

		-- Reset Failure Count
		UPDATE	dbo.Measure 
		SET		FailureCount = 0
		WHERE	ID = @Measure_ID

	END TRY
	BEGIN CATCH
		SELECT @temp_XML = 
			(SELECT 
				ERROR_MESSAGE()		AS ERROR_MESSAGE,
				ERROR_SEVERITY()	AS ERROR_SEVERITY,
				ERROR_STATE()		AS ERROR_STATE
			  FOR XML RAW)
		-- Set RowCount to -1
		SELECT @TestResultCount = -1	
		
		-- Disable Rule to Prevent More runs
		IF (@FailureCount >= 4)
			SET @Disable = 1
		UPDATE	dbo.Measure 
		SET		FailureCount = FailureCount+1, 
				Disable = @Disable
		WHERE	ID = @Measure_ID
	END CATCH

	-- Save Test Results to Execution Table
	-- ========================================================
	-- Insert into Execution
	INSERT INTO dbo.Execution (Measure_ID, TimeStamp, Assertion, ViolationCount, ViolationData)
	SELECT	@Measure_ID, CURRENT_TIMESTAMP, @Assertion, @TestResultCount, @temp_XML
	
	SELECT @Execution_ID = SCOPE_IDENTITY()

	-- Return Results
	-- ========================================================
	SELECT	@Execution_ID				AS ResponseCode,
			'Measure run successfully. ' + 
			'Violations: '		+ CAST(@TestResultCount as varchar(20)) + '. ' + 
			'Execution_ID: '	+ CAST(@Execution_ID as varchar(20)) + '. '
			AS ResponseMsg
END
GO
