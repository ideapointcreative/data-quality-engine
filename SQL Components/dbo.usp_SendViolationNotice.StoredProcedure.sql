USE [DQ]
GO
/****** Object:  StoredProcedure [dbo].[usp_SendViolationNotice]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_SendViolationNotice] (
	@execution_ID int = 0
	)
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE		@measure_id as BIGINT,
				@style as VARCHAR(MAX),
				@violation as VARCHAR(MAX),
				@trend as VARCHAR(MAX),
				@email_body as VARCHAR(MAX),
				@email_subject as VARCHAR(1000),
				@tech_details as VARCHAR(1000),
				@measure_name as VARCHAR(100),
				@description as VARCHAR(250),
				@NotificationSMTP as varchar(1000),
				@db_mail_profile as varchar(50),
				@Threshold as numeric(5,3),
				@email_from as varchar(255),
				@email_default as varchar(255)

	-- Set Measure ID
	SELECT @measure_id = Measure_ID from Execution WHERE ID = @execution_ID

	-- Load Configuration Values
	SELECT @db_mail_profile =	ParameterValue FROM [dbo].[Configuration] WHERE ParameterName = 'db_mail_profile'
	SELECT @style =				ParameterValue FROM [dbo].[Configuration] WHERE ParameterName = 'email_style'
	SELECT @email_body =		ParameterValue FROM [dbo].[Configuration] WHERE ParameterName = 'email_violation_template'
	SELECT @email_from =		ParameterValue FROM [dbo].[Configuration] WHERE ParameterName = 'email_from'
	SELECT @email_default =		ParameterValue FROM [dbo].[Configuration] WHERE ParameterName = 'email_default'

	-- Load Measure Details
	SELECT		@measure_name = name,
				@description = description,
				@NotificationSMTP = COALESCE(NotificationSMTP,@email_default),
				@Threshold = Threshold
	FROM		Measure
	WHERE		ID = @measure_id

	-- Build Body Components
	SELECT @violation =	CAST(dbo.HTMLtableFromXML((
							SELECT [ViolationData] FROM [DQ].[dbo].[Execution] WHERE ID = @execution_ID
						)) AS VARCHAR(MAX));
	SELECT @trend =		CAST(dbo.HTMLtableFromXML((
							SELECT TOP 10 TimeStamp, ViolationCount FROM dbo.Execution WHERE Measure_ID = @measure_id ORDER BY TimeStamp DESC FOR XML RAW,ELEMENTS XSINIL
						)) AS VARCHAR(MAX));
	SELECT @tech_details =	'<h3>Technical Details</h3><p>' + 
							'Measure_ID=' + CAST(@measure_id AS VARCHAR(25)) +
							'<br/>Execution_ID=' + CAST(@execution_ID AS VARCHAR(25)) + 
							'</p>'

	-- Replace Placeholders in template with Data
	SET @email_body = REPLACE(@email_body,'{measure_name}',COALESCE(@measure_name,'[none]'))
	SET @email_body = REPLACE(@email_body,'{description}',COALESCE(@description,'[none]'))
	SET @email_body = REPLACE(@email_body,'{notificationsmtp}',COALESCE(@NotificationSMTP,@email_default))
	SET @email_body = REPLACE(@email_body,'{threshold}',COALESCE(@Threshold,0))
	SET @email_body = REPLACE(@email_body,'{trend}',@trend)
	SET @email_body = REPLACE(@email_body,'{violations}',@violation)

	-- Assemble the Email Body
	SET @email_body = COALESCE(@style,'') + COALESCE(@email_body,'') + COALESCE(@tech_details,'')

	-- Assemble the Email Subject
	SET @email_subject = 'DQ Violation(s): ' + @measure_name + ' [Execution ID ' + CAST(@execution_ID AS varchar(25)) + ']'

	  EXEC msdb.dbo.sp_send_dbmail 
				@profile_name = @db_mail_profile
				, @recipients = @NotificationSMTP
				, @from_address = @email_from
				, @body = @email_body
				, @body_format = 'HTML'
				, @subject = @email_subject
				--, @file_attachments = @fullFileName;

END

GO
