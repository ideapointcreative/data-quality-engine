USE [DQ]
GO
/****** Object:  StoredProcedure [dbo].[usp_ExecuteAllMeasures]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[usp_ExecuteAllMeasures]
AS
BEGIN
	SET NOCOUNT ON;

	-- Load Rules into Cursor
	-- =============================================================
	DECLARE		@Measure_ID BIGINT
	DECLARE c1 CURSOR FOR
		SELECT	ID FROM	dbo.[Measure] WHERE	Disable = 0

	-- Loop Through Cursor
	-- =============================================================
	OPEN c1
	FETCH NEXT FROM c1 INTO @Measure_ID
	WHILE @@FETCH_STATUS = 0
	BEGIN
		EXEC [usp_ExecuteMeasure] @Measure_ID
		FETCH NEXT FROM c1 INTO @Measure_ID
	END
	CLOSE c1
	DEALLOCATE c1

END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'This stored procedures is used to run all active Measures.  This is meant to be triggered by a SQL Agent job, or another scheduling mechanism.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'PROCEDURE',@level1name=N'usp_ExecuteAllMeasures'
GO
