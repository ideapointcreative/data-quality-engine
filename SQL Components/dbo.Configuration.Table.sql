USE [DQ]
GO
/****** Object:  Table [dbo].[Configuration]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Configuration](
	[ParameterName] [varchar](50) NOT NULL,
	[ParameterValue] [varchar](max) NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Common name of the parameter' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Configuration', @level2type=N'COLUMN',@level2name=N'ParameterName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Value of the parameter.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Configuration', @level2type=N'COLUMN',@level2name=N'ParameterValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Generic table for storing configuration details required by the DQ Engine.

The following Parameters are required

FilePath - This is the default filepath in which violation extracts will be written.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'Configuration'
GO
