USE [DQ]
GO
/****** Object:  UserDefinedFunction [dbo].[rule_NonServicableFiberAddressAttribute]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rule_NonServicableFiberAddressAttribute] (
	@TestValue	varchar(40)
)
RETURNS varchar(50)
AS
BEGIN
	-- Clean the value up
	SET @TestValue = TRIM(@TestValue)
	DECLARE @ReturnValue AS VARCHAR(50) = null

		SELECT	@ReturnValue = Description
		FROM	UTIL_ODS.Lookup.PremisePurpose
		WHERE	@TestValue LIKE '%'+ ID +'%'
		
		IF (@ReturnValue IS NOT NULL)
			RETURN UPPER(@ReturnValue)
		RETURN null
END
GO
