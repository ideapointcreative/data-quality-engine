USE [DQ]
GO
/****** Object:  UserDefinedFunction [dbo].[rule_IsZipCode]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[rule_IsZipCode](
	@TestValue	varchar(100)
)
RETURNS INT
AS
BEGIN

	IF (@TestValue LIKE '[0-9][0-9][0-9][0-9][0-9]'
		OR @TestValue LIKE '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
		OR @TestValue LIKE '[0-9][0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9]'
		)
		RETURN 1
	
	RETURN 0
	
END

GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Test to determine if the zip code "appears" to be valid.  This does not confirm the validity of the zip code, only that it appears correct, by being in one of the following formats: 88888,88888888 or 88888-8888.' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'FUNCTION',@level1name=N'rule_IsZipCode'
GO
