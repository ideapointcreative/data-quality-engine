USE [DQ]
GO
/****** Object:  UserDefinedFunction [dbo].[HTMLtableFromXML]    Script Date: 9/10/2021 3:22:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[HTMLtableFromXML](@SelectForXmlRawElementsXsinil XML)
RETURNS XML
AS
BEGIN
-- Lifted from https://stackoverflow.com/questions/39404440/how-to-convert-xml-as-a-table-or-html-table-in-sqlserver
RETURN
(
    SELECT  
    @SelectForXmlRawElementsXsinil.query('let $first:=/row[1]
                return 
                <tr> 
                {
                for $th in $first/*
                return <td>{local-name($th)}</td>
                }
                </tr>') AS thead
    ,@SelectForXmlRawElementsXsinil.query('for $tr in /row
                 return 
                 <tr>
                 {
                 for $td in $tr/*
                 return <td>{string($td)}</td>
                 }
                 </tr>') AS tbody
    FOR XML PATH('table'),TYPE
);
END
GO
