-- ===============================================================
-- Data Quality Demonstration Script
-- ------------------------------------------
-- This script contains code for demonstrating how the data quality 
-- framework is utilized.

-- Requirements:
--	- Restoration of Microsoft's sample AdventureWorks2019 as
--		AdventureWorks on the same server as the DQ framework.
--	- db_datawriter perms on AdventureWorks
--
-- ===============================================================

-- Dirty up some data in Person.EmailAddress
-- ------------------------------------------
USE AdventureWorks
UPDATE Person.EmailAddress SET EmailAddress = 'roberto0@adventure-works'		WHERE BusinessEntityID = 3
UPDATE Person.EmailAddress SET EmailAddress = 'gail @adventure-works.com'		WHERE BusinessEntityID = 5
UPDATE Person.EmailAddress SET EmailAddress = 'mark1 adventure-works.com'		WHERE BusinessEntityID = 29
UPDATE Person.EmailAddress SET EmailAddress = 'brandon0@adventure works.com'	WHERE BusinessEntityID = 35
UPDATE Person.EmailAddress SET EmailAddress = 'maciej0'							WHERE BusinessEntityID = 63
UPDATE Person.EmailAddress SET EmailAddress = 'betsy0@adventureworks.com'		WHERE BusinessEntityID = 88
UPDATE Person.EmailAddress SET EmailAddress = 'mary1@adventure-works.net'		WHERE BusinessEntityID = 104


-- Show All Data
-- ------------------------------------------
USE DQ;
SELECT		BusinessEntityID,EmailAddress
FROM		AdventureWorks.Person.EmailAddress
ORDER BY	BusinessEntityID

-- All Records which violate the rule (fail assertion)
-- ------------------------------------------
USE DQ;
SELECT		BusinessEntityID,EmailAddress
FROM		AdventureWorks.Person.EmailAddress
WHERE		dbo.rule_IsEmail(EmailAddress) = 0
ORDER BY	BusinessEntityID


-- Create a Measure
-- ------------------------------------------
USE DQ;
INSERT INTO [dbo].[Measure]
           ([Name]
           ,[Description]
           ,[Threshold]
           ,[Category_ID]
           ,[NotificationSMTP]
           ,[Assertion]
           )
SELECT	'AdventureWorks - Invalid Email Addresses',
        'Tests all addresses in the database, to ensure correct formatting.',
        2,
        (SELECT ID FROM CATEGORY WHERE [Name] = 'Demo'),
        '',
        '	SELECT		BusinessEntityID,EmailAddress
			FROM		AdventureWorks.Person.EmailAddress
			WHERE		dbo.rule_IsEmail(EmailAddress) = 0
			ORDER BY	BusinessEntityID		   
		'


-- Run the Measures
-- ------------------------------------------
USE DQ;
EXEC usp_ExecuteAllMeasures


-- Demo of Data Cleansing using Rule
-- ------------------------------------------
USE DQ;
SELECT		BusinessEntityID,
			CASE dbo.rule_IsEmail(EmailAddress)
			WHEN 1	THEN EmailAddress
			ELSE	'N/A'
			END AS EmailAddress
FROM		AdventureWorks.Person.EmailAddress
ORDER BY	BusinessEntityID