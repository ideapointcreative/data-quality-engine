# Data Quality Engine #

This is a simple implementation of a Data Quality Measure Engine for Microsoft SQL.  It allows for creation of MEASUREs, 
which are basically SQL SELECT statements looking for Data Quality Validations.

When a measure it tested, a count of the violations and the data returned from the test is saved to the EXECUTION table.  
Using the data in the EXECUTION, allows you to develop trending reports.

The engine is designed to be run/scheduled using SQL Agent.


## Data Model ##
![picture](img/Tables.jpg)


## How To Use/Build ##
* Clone the repo locally
* Run the SQL script in **Build Script** directory.


## FAQ ##

### How do I write Measures? ###
Rules are meant to be simple SQL SELECT statements that look for Data Quality Violations.  The queries are executed in the context of the Data Quality database, so proper three (or four) part object prefixing is required.

Say for instance, your Customer Service department wants to ensure that all active customers have phone numbers.  You could write a simple test like below.

	SELECT CustomerID FROM Customer_DB.dbo.t_CUSTOMERS WHERE PhoneNumber IS NULL AND ACTIVE = 1
	
If the measure requires more complex logic, it is suggested that a VIEW be created in the CUSTOM schema, and that view be referenced in the MEASURE
	
	
### How do I run a rule? ###
Rules can be run individually, by invoking the uspExecuteRule proceedure

	EXEC uspExecuteRule <Rule_ID>
	
If you would like to run ALL rules, then run the following.  This is also what you should schedule using SQLagent.

	EXEC usp_ExecuteAllMeasures

### How do I see the results/violations of a rule?###
The following will return an HTML table of the violations of a measure.  This is meant to be used in reporting and email notifications.
	
	SELECT	dbo.HTMLtableFromXML((
			SELECT [ViolationData] FROM [DQ].[dbo].[Execution] WHERE ID = [#]
		)) AS Violations


