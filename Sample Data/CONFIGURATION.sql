USE [DQ]
GO
INSERT [dbo].[Configuration] ([ParameterName], [ParameterValue]) VALUES (N'db_mail_profile', N'DB-Mail')
INSERT [dbo].[Configuration] ([ParameterName], [ParameterValue]) VALUES (N'email_style', N'<style>
* {
	font-family: arial;
	padding: 0;
	}
table {
	border: 1px solid #000;
	border-collapse: collapse;
	font-size: 10pt;
	}
td,th {
	padding:2px 4px;
	border: 1px solid #000;
	}
thead {
	font-weight: bold;
	background-color: #ccc
	}
</style>')
INSERT [dbo].[Configuration] ([ParameterName], [ParameterValue]) VALUES (N'email_violation_template', N'<h1>{measure_name}</h1>
<p>This message is to alert you that Data Quality (DQ) violations exist against this measure. </p>
<ul>
	<li><b>Measure Name:</b> {measure_name}</li>
	<li><b>Measure Description:</b> {description}</li>
	<li><b>Data Steward(s):</b> {notificationsmtp}</li>
	<li><b>Violation Threshold:</b> {threshold}</li>
</ul>

<h2>Violations Trend</h2>
<p>Below you will see the violation counts for this Data Quality Measure over the past 10 executions. Use this to identify trends.</p>
{trend}
<h2>Violations from Last Execution</h2>
<p>The following records have violated this data quality measure. Please investigate.</p>
{violations}')
INSERT [dbo].[Configuration] ([ParameterName], [ParameterValue]) VALUES (N'email_from', N'no-reply@yourcompany.com')
INSERT [dbo].[Configuration] ([ParameterName], [ParameterValue]) VALUES (N'email_default', N'dqadmin@yourcompany.com')
